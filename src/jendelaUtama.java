
import absensi.dataTransaksi;
import absensi.dataTransaksiBermasalah;
import absensi.laporanAbsensi;
import absensi.prosesData;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UnsupportedLookAndFeelException;
import master.createUser;
import utility.ImagePanelFix;
import utility.getConnection;
import utility.penangananKomponen;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author iweks
 */
public class jendelaUtama extends javax.swing.JFrame {

    Connection kon;
    java.awt.CardLayout cardMenu = new java.awt.CardLayout();
    public java.awt.CardLayout cardTengah = new java.awt.CardLayout();
    String user = "";
    getConnection u;
    penangananKomponen kom;

    pengaturan p_pengaturan;
    Image image = null;
    String folder;
    createUser createUser;
    prosesData prosesData;
    laporanAbsensi laporanAbsensi;
    dataTransaksi dataTransaksi;
    dataTransaksiBermasalah dataTransaksiBermasalah;

    /**
     * Creates new form jendelaUtama
     */
    public jendelaUtama() {
        u = new getConnection();
        kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        kom = new penangananKomponen();

        if (kon == null) {
            setVisible(false);
            new pengaturan();
        } else {
            folder = System.getProperty("user.dir");
            try {
                image = ImageIO.read(new File(folder + "\\image\\bg.png"));
            } catch (IOException ex) {
                Logger.getLogger(jendelaUtama.class.getName()).log(Level.SEVERE, null, ex);
            }
            initComponents();
            java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screen);
            //    toolbar.setVisible(false);
            //    menuBar.setVisible(false);
            final DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
            final DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
            ActionListener taskPerformer = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {

                    java.util.Date date = new java.util.Date();

                    String datestring = dateFormat.format(date);
                    String datestring2 = dateFormat2.format(date);
                    jam.setText(datestring);
                    jam1.setText(datestring2);

                }
            };
            new Timer(1000, taskPerformer).start();

            try {
                this.setIconImage(ImageIO.read(new File(folder + "\\image\\icon.jpg")));
            } catch (IOException ex) {
                //     Logger.getLogger(awal.class.getName()).log(Level.SEVERE, null, ex);
            }
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    tutup();
                }
            });
            setLocation(0, 0);
            setVisible(true);
            tampilMenuTengah();
            tampil();
        }
    }

    private void tutup() {
        int yyy = JOptionPane.showConfirmDialog(this, "Anda Yakin ingin menutup Aplikasi?", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (yyy == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    private void tampil() {
        java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int wt = 380;
        int ht = 170;
        int w = screen.width;
        int h = screen.height;
        login.setLocation((w - wt) / 2, ((h - ht) / 2) - (ht / 2));
        login.setSize(wt, ht);
        d_user.requestFocus();
        login.setVisible(true);
    }

    public void showFrame(JInternalFrame jss, String judul) {

        boolean frame = this.isLoaded(judul);
        Dimension d = tengah.getSize();
        if (!frame) {
            try {
                //     slideMenu.add(addPanel(judul, judul, jss));

                jss.setSize(d);

                this.tengah.add(jss);
                jss.setMaximum(true);
                jss.show();
                jss.setSelected(true);

            } catch (java.beans.PropertyVetoException e) {
            }
        }
    }

    public boolean isLoaded(String FormTitle) {
        javax.swing.JInternalFrame Form[] = this.tengah.getAllFrames();
        for (int i = 0; i < Form.length; i++) {
            if (Form[i].getTitle().equalsIgnoreCase(FormTitle)) {
                Form[i].setLocation(0, 0);

                Form[i].show();

                try {
                    Form[i].setIcon(false);
                    Form[i].setSelected(true);
                } catch (java.beans.PropertyVetoException e) {
                }
                return true;
            }
        }
        return false;
    }

    private boolean login(String user, String pass) {
        boolean hasil = false;
        int a = 0;
        String sql = "SELECT count(userid) FROM tvri_pengguna WHERE userid = '" + user + "' AND sandi = MD5('" + pass + "')";

        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String aa = rset.getString(1);
                a = Integer.parseInt(aa);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (a > 0) {
            hasil = true;
        }

        return hasil;
    }

    private void goLogin() {
        String a = d_user.getText().toLowerCase();
        String b = d_pass.getText().toLowerCase();
        boolean ok = login(a, b);
        if (ok) {
            suksesLogin();
            login.dispose();
        } else {
            JOptionPane.showMessageDialog(login, "Maaf, User ID dan password tidak sesuai");
            d_user.setText("");
            d_pass.setText("");
            d_user.requestFocus();
        }
    }

    private boolean backupDataWithOutDatabase() {
        String dumpExePath = folder + "\\backup\\mysqldump.exe";
        String backupPath = folder;
        boolean status = false;
        try {
            Process p = null;

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String filepath = "\\backup\\ftm-" + u.host + "-(" + dateFormat.format(date) + ").sql";

            String batchCommand = "";
            if (u.pass != "") {
                //only backup the data not included create database
                batchCommand = dumpExePath + " -h " + u.host + " --port 3306 -u " + u.user + " --password=" + u.pass + " " + u.dbNama + " -r \"" + backupPath + "" + filepath + "\"";
            } else {
                batchCommand = dumpExePath + " -h " + u.host + " --port 3306 -u " + u.user + " " + u.dbNama + " -r \"" + backupPath + "" + filepath + "\"";
            }

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();

            if (processComplete == 0) {
                status = true;
                //  log.info("Backup created successfully for without DB " + database + " in " + host + ":" + port);
            } else {
                status = false;
                //  log.info("Could not create the backup for without DB " + database + " in " + host + ":" + port);
            }

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return status;
    }

    private void suksesLogin() {
        user = this.d_user.getText();
        menuAdmin.setVisible(false);
        menuPresensi.setVisible(false);

        String ab = kom.getStringSQL(kon, "SELECT peran FROM tvri_pengguna WHERE userid = '" + user + "'");
        if (ab.length() == 0) {
            ab = "Super Admin";
        }
        if (ab.equalsIgnoreCase("Admin Laporan")) {

            laporanAbsensi = new laporanAbsensi(user, kon);
            showFrame(laporanAbsensi, "Laporan Absensi");
        } else {
            menuBar.setVisible(true);
            dataTransaksi = new dataTransaksi(user, kon);
            showFrame(dataTransaksi, ".:: Presensi Pegawai ::.");
            if (ab.equalsIgnoreCase("Super Admin")) {
                menuAdmin.setVisible(true);
                menuPresensi.setVisible(true);
            } else {
                menuPresensi.setVisible(true);
            }
        }

        d_user.setText("");
        d_pass.setText("");
    }

    private void tampilMenuTengah() {
        this.tengah.removeAll();
        this.tengah.add(menuTengah);
        menuTengah.setSize(tengah.getSize().width, tengah.getSize().height);
        menuTengah.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        login = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        d_user = new javax.swing.JTextField();
        d_pass = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        b_masuk = new javax.swing.JButton();
        b_keluar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        menuTengah = new javax.swing.JPanel();
        menuTengah = new ImagePanelFix();
        jPanel1 = new javax.swing.JPanel();
        jam2 = new javax.swing.JLabel();
        jam = new javax.swing.JLabel();
        jam1 = new javax.swing.JLabel();
        tengah = new javax.swing.JDesktopPane();
        menuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        menuPresensi = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        menuAdmin = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        login.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        login.setTitle("LOGIN");
        login.setModal(true);
        login.setResizable(false);
        login.getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/login1.jpg"))); // NOI18N
        jPanel4.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        d_user.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_userKeyReleased(evt);
            }
        });
        jPanel4.add(d_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 150, -1));

        d_pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_passKeyReleased(evt);
            }
        });
        jPanel4.add(d_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 150, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("User ID");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Password");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, -1, 20));

        jPanel5.setBackground(new java.awt.Color(255, 51, 51));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        b_masuk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        b_masuk.setText("Masuk");
        b_masuk.setPreferredSize(new java.awt.Dimension(83, 25));
        b_masuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_masukActionPerformed(evt);
            }
        });
        jPanel5.add(b_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, -1, -1));

        b_keluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        b_keluar.setText("Keluar");
        b_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_keluarActionPerformed(evt);
            }
        });
        jPanel5.add(b_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, -1));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/setting.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, -1));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 390, 50));

        login.getContentPane().add(jPanel4);

        menuTengah.setBackground(new java.awt.Color(102, 102, 255));
        menuTengah.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                menuTengahFocusGained(evt);
            }
        });
        menuTengah.setLayout(new java.awt.BorderLayout());

        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 175));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 50, 0));

        jam2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jam2.setPreferredSize(new java.awt.Dimension(88, 35));
        jPanel1.add(jam2);

        jam.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jam.setForeground(new java.awt.Color(0, 51, 204));
        jPanel1.add(jam);

        jam1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jam1.setForeground(new java.awt.Color(0, 51, 204));
        jPanel1.add(jam1);

        menuTengah.add(jPanel1, java.awt.BorderLayout.EAST);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Sistem Absensi Pegawai TVRI Jawa Tengah");
        setExtendedState(this.MAXIMIZED_BOTH);

        tengah.setBackground(new java.awt.Color(153, 153, 255));

        javax.swing.GroupLayout tengahLayout = new javax.swing.GroupLayout(tengah);
        tengah.setLayout(tengahLayout);
        tengahLayout.setHorizontalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1349, Short.MAX_VALUE)
        );
        tengahLayout.setVerticalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 621, Short.MAX_VALUE)
        );

        getContentPane().add(tengah, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Sistem");
        jMenu1.setToolTipText("");

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logout.png"))); // NOI18N
        jMenuItem7.setText("Log Out");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jMenuItem8.setText("Tutup Aplikasi");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem8);

        menuBar.add(jMenu1);

        menuPresensi.setText("Presensi Pegawai");

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/export.png"))); // NOI18N
        jMenuItem3.setText("Proses Presensi");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuPresensi.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/rubah.png"))); // NOI18N
        jMenuItem4.setText("Data Presensi Pegawai");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        menuPresensi.add(jMenuItem4);

        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jMenuItem9.setText("Presensi Pegawai Bermasalah");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        menuPresensi.add(jMenuItem9);

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/order.png"))); // NOI18N
        jMenuItem5.setText("Laporan Presensi");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        menuPresensi.add(jMenuItem5);

        menuBar.add(menuPresensi);

        menuAdmin.setText("Admin");

        jMenuItem1.setText("Pengguna");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        menuAdmin.add(jMenuItem1);

        jMenuItem2.setText("Backup Database");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAdmin.add(jMenuItem2);

        jMenuItem6.setText("Pengaturan Koneksi");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAdmin.add(jMenuItem6);

        menuBar.add(menuAdmin);

        setJMenuBar(menuBar);

        setBounds(0, 0, 1367, 694);
    }// </editor-fold>//GEN-END:initComponents

    private void d_userKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_userKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            d_pass.requestFocus();
        }
    }//GEN-LAST:event_d_userKeyReleased

    private void d_passKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_passKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            goLogin();
        }
    }//GEN-LAST:event_d_passKeyReleased

    private void b_masukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_masukActionPerformed
        goLogin();
    }//GEN-LAST:event_b_masukActionPerformed

    private void b_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_keluarActionPerformed

        System.exit(1);
    }//GEN-LAST:event_b_keluarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        login.dispose();
        setVisible(false);
        new pengaturan();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void menuTengahFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_menuTengahFocusGained
        JOptionPane.showConfirmDialog(this, "OWOWOWOW");
    }//GEN-LAST:event_menuTengahFocusGained

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        //   if (prosesData == null) {
        prosesData = new prosesData(user, kon);
        //    }
        showFrame(prosesData, "Proses Data");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // if (laporanAbsensi == null) {
        laporanAbsensi = new laporanAbsensi(user, kon);
        //    }
        showFrame(laporanAbsensi, "Laporan Absensi");
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        dataTransaksi = new dataTransaksi(user, kon);
        //    }
        showFrame(dataTransaksi, ".:: Presensi Pegawai ::.");
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        createUser = new createUser(user, kon);
        //    }
        showFrame(createUser, "Create User");
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        int yy = JOptionPane.showConfirmDialog(this, "Anda yakin ingin memulai proses Backup Database?", "konfirmasi", JOptionPane.YES_NO_OPTION);
        if (yy == JOptionPane.YES_OPTION) {
            boolean ok = backupDataWithOutDatabase();
            if (ok) {
                JOptionPane.showMessageDialog(this, "SUKSES BACKUP DATABASE di FOLDER : " + folder);
            } else {
                JOptionPane.showMessageDialog(this, "GAGAL BACKUP DATABASE... HUB ADMIN");
            }
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed

        this.dispose();
        new pengaturan();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        int yyy = JOptionPane.showConfirmDialog(this, "Anda Yakin ingin menutup Aplikasi?", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (yyy == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed

        this.tengah.removeAll();
        this.repaint();
        tampil();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        dataTransaksiBermasalah = new dataTransaksiBermasalah(user, kon);
        //    }
        showFrame(dataTransaksiBermasalah, ".:: Presensi Pegawai Bermasalah ::.");
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel("org.fife.plaf.Office2003.Office2003LookAndFeel");
            //   javax.swing.UIManager.setLookAndFeel("org.fife.plaf.OfficeXP.OfficeXPLookAndFeel");
            // JFrame.setDefaultLookAndFeelDecorated(true);
            new jendelaUtama();

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_keluar;
    private javax.swing.JButton b_masuk;
    private javax.swing.JPasswordField d_pass;
    private javax.swing.JTextField d_user;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel jam;
    private javax.swing.JLabel jam1;
    private javax.swing.JLabel jam2;
    private javax.swing.JDialog login;
    private javax.swing.JMenu menuAdmin;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuPresensi;
    private javax.swing.JPanel menuTengah;
    private javax.swing.JDesktopPane tengah;
    // End of variables declaration//GEN-END:variables
}
