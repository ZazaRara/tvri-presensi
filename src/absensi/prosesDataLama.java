/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absensi;

import java.sql.Connection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class prosesDataLama extends javax.swing.JInternalFrame {

    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    String user = "";
    Connection kon;
    TaskWorker task;

    /**
     * Creates new form prosesData
     */
    public prosesDataLama(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        atas.removeAll();
        atas.add(panelFilter);
        setJamAbsensi();
    }

    private synchronized void hapusDataAbsensi(String awal, String akhir) {
        String sql = "DELETE FROM tvri_pegawai_data_absensi WHERE tanggal BETWEEN '" + awal + "' AND '" + akhir + "'";
        komp.setSQL(kon, sql);
    }

    private void setJamAbsensi() {
        String sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'PAGI-IN'";
        Object[] data = komp.setDataEdit(kon, sql);
        j_pagi_in_awal.setText(data[0].toString());
        j_pagi_in_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'PAGI-OUT'";
        data = komp.setDataEdit(kon, sql);
        j_pagi_out_awal.setText(data[0].toString());
        j_pagi_out_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'SIANG-IN'";
        data = komp.setDataEdit(kon, sql);
        j_siang_in_awal.setText(data[0].toString());
        j_siang_in_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'SIANG-OUT'";
        data = komp.setDataEdit(kon, sql);
        j_siang_out_awal.setText(data[0].toString());
        j_siang_out_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'MALAM-OUT'";
        data = komp.setDataEdit(kon, sql);
        j_malam_out_awal.setText(data[0].toString());
        j_malam_out_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'MALAM-IN'";
        data = komp.setDataEdit(kon, sql);
        j_malam_in_awal.setText(data[0].toString());
        j_malam_in_akhir.setText(data[1].toString());

    }

    private synchronized void simpanAbsensi() {
        int yy = tabelData.getRowCount();
        System.out.println(yy);
        for (int x = 0; x < yy; x++) {
            String a = tabelData.getValueAt(x, 1).toString();
            String b = tabelData.getValueAt(x, 2).toString();
            String c = tabelData.getValueAt(x, 3).toString();
            String d = tabelData.getValueAt(x, 4).toString();
            String e = tabelData.getValueAt(x, 5).toString();
            String f = tabelData.getValueAt(x, 6).toString();
            String g = tabelData.getValueAt(x, 7).toString();
            String h = tabelData.getValueAt(x, 8).toString();
            String i = tabelData.getValueAt(x, 9).toString();
            String j = tabelData.getValueAt(x, 10).toString();
            String k = tabelData.getValueAt(x, 11).toString();
            String m = tabelData.getValueAt(x, 12).toString();
            String save = "INSERT INTO tvri_pegawai_data_absensi"
                    + "(id_pegawai, no_absen, keterangan, tanggal, jam_pagi, "
                    + "jam_siang, jam_sore_in, jam_sore, jam_malam_in, jam_malam, asn, non_asn, sort_tanggal, from_date) "
                    + "VALUES('" + a + "','" + b + "','" + c + "','" + d + "',"
                    + "'" + e + "','" + f + "','" + g + "','" + h + "','" + i + "','" + j + "','" + k + "','" + m + "','" + d.replaceAll("-", "") + "',now())";
            //   System.out.println(a);
            komp.setSQL(kon, save);
        }
    }

    private void updateJamAbsensi() {

        String a = j_pagi_in_awal.getText();
        String b = j_pagi_in_akhir.getText();
        String c = "PAGI-IN";
        String sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        a = j_pagi_out_awal.getText();
        b = j_pagi_out_akhir.getText();
        c = "PAGI-OUT";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        a = j_siang_in_awal.getText();
        b = j_siang_in_akhir.getText();
        c = "SIANG-IN";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        a = j_siang_out_awal.getText();
        b = j_siang_out_akhir.getText();
        c = "SIANG-OUT";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        a = j_malam_in_awal.getText();
        b = j_malam_in_akhir.getText();
        c = "MALAM-IN";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        a = j_malam_out_awal.getText();
        b = j_malam_out_akhir.getText();
        c = "MALAM-OUT";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

    }

    private void prosesDataAbsensi() {
        String awal = new java.sql.Date(tglAwal.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tglAkhir.getDate().getTime()).toString();
        updateJamAbsensi();
        hapusDataAbsensi(awal, akhir);

        String sql = "SELECT peg.emp_id_auto as id_pegawai,a.pin,peg.first_name as nama, a.tgl, awal.jam as jam_pagi, jeda.jam as jam_siang,siang.jam as jam_siang_in, keluar.jam as jam_sore,malam_in.jam as malam_in, malam.jam as jam_malam, \n"
                + "CASE WHEN malam.jam IS NOT NULL THEN cast('16:00:00' as time)  - awal.jam ELSE keluar.jam - awal.jam END / 10000 as asn, "
                + "CASE WHEN keluar.jam IS NULL AND awal.jam IS NOT NULL THEN malam.jam - cast('16:00:00' as time) ELSE malam.jam  - keluar.jam END / 10000 as opr\n"
                + "FROM (\n"
                + "    SELECT DISTINCT pin, cast(scan_date as date) as tgl, \n"
                + "      CONCAT(cast(scan_date as date), pin) as kodetgl \n"
                + "       FROM att_log \n"
                + "       WHERE scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0\n"
                + "       ORDER BY pin, scan_date\n"
                + "    ) a\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_pagi_in_awal.getText().trim() + "' AND '" + j_pagi_in_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as awal ON awal.pin = a.pin AND awal.tgl = a.kodetgl AND awal.RowNumber = 1\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_pagi_out_awal.getText().trim() + "' AND '" + j_pagi_out_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as jeda ON jeda.pin = a.pin AND jeda.tgl = a.kodetgl AND jeda.RowNumber = 1\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_siang_out_awal.getText().trim() + "' AND '" + j_siang_out_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as keluar ON keluar.pin = a.pin AND keluar.tgl = a.kodetgl AND keluar.RowNumber = 1\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_malam_out_awal.getText().trim() + "' AND '" + j_malam_out_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as malam ON malam.pin = a.pin AND malam.tgl = a.kodetgl AND malam.RowNumber = 1 \n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_siang_in_awal.getText().trim() + "' AND '" + j_siang_in_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as siang ON siang.pin = a.pin AND siang.tgl = a.kodetgl AND siang.RowNumber = 1\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "scan_date BETWEEN '" + awal + "' AND '" + akhir + "' AND rowguid != 0 AND cast(scan_date as time) BETWEEN '" + j_malam_in_awal.getText().trim() + "' AND '" + j_malam_in_akhir.getText().trim() + "' ORDER BY pin, scan_date\n"
                + ") as malam_in ON malam_in.pin = a.pin AND malam_in.tgl = a.kodetgl AND siang.RowNumber = 1\n"
                + "LEFT JOIN emp peg ON peg.pin = a.pin "
                + "WHERE awal.jam IS NOT NULL OR jeda.jam IS NOT NULL OR keluar.jam IS NOT NULL OR malam.jam IS NOT NULL OR malam_in.jam IS NOT NULL OR siang.jam IS NOT NULL "
                + "ORDER BY a.pin";
        //     String save = "INSERT INTO tvri_pegawai_data_absensi(id_pegawai, keterangan, no_absen, tanggal, jam_pagi, jam_siang, jam_sore, jam_malam, asn, non_asn) " + sql;
        //     System.out.println(save);
        //     komp.setSQL(kon, save);

        komp.setDataTabel(kon, tabelData, sql, 1);
        simpanAbsensi();
        int yy = tabelData.getRowCount();
        if (yy == 0) {
            pesan.pesanError("Tidak Ada data Presensi", "Tanggal " + awal + " s/d " + akhir + "", "Silahkan pilih tanggal lainnya");
        } else {
            pesan.pesanSukses("Sukses Proses Data Presensi", "Tanggal " + awal + " s/d " + akhir + "<br />Total Data :" + yy);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        xxxyy = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        atas = new javax.swing.JPanel();
        panelFilter = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tglAkhir = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        j_pagi_in_akhir = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        j_pagi_in_awal = new javax.swing.JTextField();
        tglAwal = new com.toedter.calendar.JDateChooser();
        j_siang_in_awal = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        j_siang_in_akhir = new javax.swing.JTextField();
        j_pagi_out_awal = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        j_pagi_out_akhir = new javax.swing.JTextField();
        j_siang_out_awal = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        j_siang_out_akhir = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        j_malam_in_awal = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        j_malam_in_akhir = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        j_malam_out_awal = new javax.swing.JTextField();
        j_malam_out_akhir = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        panelLoading = new javax.swing.JPanel();
        loding = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();

        setTitle("Proses Data");
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        xxxyy.setBackground(new java.awt.Color(255, 255, 255));
        xxxyy.setLayout(new java.awt.BorderLayout());

        data.setOpaque(false);
        data.setLayout(new java.awt.GridLayout(1, 0));

        jScrollPane1.setOpaque(false);

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "PIN", "Nama Pegawai", "Tanggal", "Scan Pagi IN", "Scan Pagi OUT", "Scan Siang IN", "Scan Siang OUT", "Scan Malam IN", "Scan Malam OUT", "Jam ASN", "Jam Operasional"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, true, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelData.setOpaque(false);
        jScrollPane1.setViewportView(tabelData);

        data.add(jScrollPane1);

        xxxyy.add(data, java.awt.BorderLayout.CENTER);

        atas.setOpaque(false);
        atas.setLayout(new java.awt.GridLayout(1, 0));

        panelFilter.setOpaque(false);
        panelFilter.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        panelFilter.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 0, 150, 190));

        tglAkhir.setDate(new Date());
        tglAkhir.setDateFormatString("dd/MM/yyy");
        panelFilter.add(tglAkhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 90, -1));

        jLabel2.setText("s/d");
        panelFilter.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 40, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Siang OUT");
        panelFilter.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 70, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText(":");
        panelFilter.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 70, 20, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Pagi IN");
        panelFilter.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 70, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Siang IN");
        panelFilter.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 70, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Pagi OUT");
        panelFilter.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, 70, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Tanggal");
        panelFilter.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 70, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText(":");
        panelFilter.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 20, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText(":");
        panelFilter.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 20, 20));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText(":");
        panelFilter.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 20, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText(":");
        panelFilter.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, 20, 20));

        j_pagi_in_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_in_akhir.setText("08:00");
        panelFilter.add(j_pagi_in_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 90, -1));

        jLabel13.setText("s/d");
        panelFilter.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, -1, 20));

        j_pagi_in_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_in_awal.setText("08:00");
        panelFilter.add(j_pagi_in_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 90, -1));

        tglAwal.setDate(new Date());
        tglAwal.setDateFormatString("dd/MM/yyy");
        panelFilter.add(tglAwal, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 90, -1));

        j_siang_in_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_in_awal.setText("08:00");
        panelFilter.add(j_siang_in_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 90, -1));

        jLabel14.setText("s/d");
        panelFilter.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 70, -1, 20));

        j_siang_in_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_in_akhir.setText("08:00");
        panelFilter.add(j_siang_in_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 70, 90, -1));

        j_pagi_out_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_out_awal.setText("08:00");
        panelFilter.add(j_pagi_out_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 40, 90, -1));

        jLabel15.setText("s/d");
        panelFilter.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, -1, 20));

        j_pagi_out_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_out_akhir.setText("08:00");
        panelFilter.add(j_pagi_out_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 40, 90, -1));

        j_siang_out_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_out_awal.setText("08:00");
        panelFilter.add(j_siang_out_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 70, 90, -1));

        jLabel16.setText("s/d");
        panelFilter.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 100, -1, 20));

        j_siang_out_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_out_akhir.setText("08:00");
        panelFilter.add(j_siang_out_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 70, 90, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/order.png"))); // NOI18N
        jButton1.setText("Proses Data Presensi");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panelFilter.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 140, -1, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Malam IN");
        panelFilter.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 70, 20));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText(":");
        panelFilter.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 20, 20));

        j_malam_in_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_malam_in_awal.setText("08:00");
        panelFilter.add(j_malam_in_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 90, -1));

        jLabel19.setText("s/d");
        panelFilter.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, -1, 20));

        j_malam_in_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_malam_in_akhir.setText("08:00");
        panelFilter.add(j_malam_in_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 100, 90, -1));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setText("Malam OUT");
        panelFilter.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 100, 70, 20));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText(":");
        panelFilter.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 100, 20, 20));

        j_malam_out_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_malam_out_awal.setText("08:00");
        panelFilter.add(j_malam_out_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 100, 90, -1));

        j_malam_out_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_malam_out_akhir.setText("08:00");
        panelFilter.add(j_malam_out_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 100, 90, -1));

        jLabel22.setText("s/d");
        panelFilter.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 70, -1, 20));

        atas.add(panelFilter);

        panelLoading.setOpaque(false);

        loding.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/load.gif"))); // NOI18N
        panelLoading.add(loding);

        atas.add(panelLoading);

        xxxyy.add(atas, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        xxxyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setOpaque(false);
        xxxyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        xxxyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        getContentPane().add(xxxyy);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        atas.removeAll();
        atas.add(panelLoading);
        task = new TaskWorker();
        task.execute();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel atas;
    private javax.swing.JPanel data;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField j_malam_in_akhir;
    private javax.swing.JTextField j_malam_in_awal;
    private javax.swing.JTextField j_malam_out_akhir;
    private javax.swing.JTextField j_malam_out_awal;
    private javax.swing.JTextField j_pagi_in_akhir;
    private javax.swing.JTextField j_pagi_in_awal;
    private javax.swing.JTextField j_pagi_out_akhir;
    private javax.swing.JTextField j_pagi_out_awal;
    private javax.swing.JTextField j_siang_in_akhir;
    private javax.swing.JTextField j_siang_in_awal;
    private javax.swing.JTextField j_siang_out_akhir;
    private javax.swing.JTextField j_siang_out_awal;
    private javax.swing.JLabel loding;
    private javax.swing.JPanel panelFilter;
    private javax.swing.JPanel panelLoading;
    private javax.swing.JTable tabelData;
    private com.toedter.calendar.JDateChooser tglAkhir;
    private com.toedter.calendar.JDateChooser tglAwal;
    private javax.swing.JPanel xxxyy;
    // End of variables declaration//GEN-END:variables

    class TaskWorker extends SwingWorker<Void, Void> {

        /*
     * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {

            prosesDataAbsensi();
            return null;
        }

        /*
     * Executed in event dispatching thread
         */
        @Override
        public void done() {
            atas.removeAll();
            atas.add(panelFilter);

        }
    }
}
