/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absensi;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import utility.Item;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class dataTransaksi extends javax.swing.JInternalFrame {

    String user = "";
    Connection kon;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();

    /**
     * Creates new form dataTransaksi
     */
    public dataTransaksi(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        komp.setJComboBoxListVektor(unit, "SELECT cab_id_auto, cab_name FROM cabang WHERE cab_name != 'Kantor Pusat' ORDER BY cab_name", "-- SEMUA UNIT KERJA --", kon);
        //    komp.setJComboBoxListVektor(pStatus, "SELECT kode, nilai FROM tvri_list WHERE tipe = 'PEGAWAI' ORDER BY urutan", "-- Pilih Status --", kon);
        //    komp.setJComboBoxListVektor(pGol, "SELECT kode, nilai FROM tvri_list WHERE tipe = 'GOL' ORDER BY urutan", "-- Pilih Pangkat/Golongan --", kon);
        komp.setJComboBoxList(pStatus, "PEGAWAI", "-- Pilih Status --", kon);
        komp.setJComboBoxList(pGol, "GOL", "-- Pilih Pangkat/Golongan --", kon);
        komp.setJComboBoxList(preStatus, "PRESENSI", "Tidak Masuk", kon);
        komp.setJComboBoxList(preStatus1, "PRESENSI", "Tidak Masuk", kon);
        btnAbsensi.setEnabled(false);
        btnPribadi.setEnabled(false);
        btnCetak.setEnabled(false);
        pId.setVisible(false);
        //     atas.removeAll();
        //     atas.add(panelFilter);
        //    setJamAbsensi();
    }

    private void detailPresensi(String id) {
        panelDetailPresensi.setVisible(false);
        String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();

        detId.setText(id);
        detId.setVisible(false);
        komp.setSQL(kon, "SET lc_time_names = 'id_ID';");
        String sql = "SELECT IFNULL(abs.id, 'KOSONG') as id, DAYNAME(c.tanggal) as hari, c.tanggal as tgl, \n"
                + "abs.jam_pagi as masuk, abs.jam_sore as pulang, LEFT(abs.asn, 5) as durasi,\n"
                + "CASE WHEN (abs.status_absensi = 'Masuk') THEN 'Masuk'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 6 THEN ''\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 5 THEN ''\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND libur.holiday_date IS NOT NULL THEN ''\n"
                //   + "WHEN (abs.status_absensi = 'Masuk') THEN 'Masuk'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND abs.status_absensi IS NULL THEN 'Tidak Masuk'\n"
                + "ELSE abs.status_absensi\n"
                + "END as status_absensi,\n"
                + "CASE WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 6 THEN 'Hari Minggu'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 5 THEN 'Hari Sabtu'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND libur.holiday_date IS NOT NULL THEN libur.holiday_note\n"
                //       + "WHEN (asn = '-' OR asn IS NULL) AND ex.alasan IS NOT NULL THEN ex.alasan\n"
                + "ELSE abs.keterangan\n"
                + "END as ket\n"
                + "FROM(\n"
                + "select selected_date as tanggal from\n"
                + "(select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1)v\n"
                + "where selected_date between '" + awal + "' and '" + akhir + "') c\n"
                + "LEFT JOIN tvri_pegawai_data_absensi abs ON abs.tanggal = c.tanggal AND abs.id_pegawai = '" + id + "'\n"
                + "LEFT JOIN holiday libur ON libur.holiday_date = c.tanggal\n"
                //+ "LEFT JOIN ex_absent ex ON CAST(ex.tgl_awal as date) = c.tanggal AND ex.emp_id_auto = abs.id_pegawai\n"
                + "ORDER BY c.tanggal";

        komp.setDataTabel(kon, tabelDetail, sql, 1);
    }

    private void detailPresensi1(String id) {
        panelDetailPresensi.setVisible(false);
        String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();

        detId1.setText(id);
        detId1.setVisible(false);
        komp.setSQL(kon, "SET lc_time_names = 'id_ID'");
        String sql = "SELECT IFNULL(abs.id, 'KOSONG') as id,'false' as cek, DAYNAME(c.tanggal) as hari, c.tanggal as tgl, \n"
                + "abs.jam_pagi as masuk, abs.jam_sore as pulang, LEFT(abs.asn, 5) as durasi,\n"
                + "CASE WHEN (abs.status_absensi = 'Masuk') THEN 'Masuk'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 6 THEN ''\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 5 THEN ''\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND libur.holiday_date IS NOT NULL THEN ''\n"
                //   + "WHEN (abs.status_absensi = 'Masuk') THEN 'Masuk'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND abs.status_absensi IS NULL THEN 'Tidak Masuk'\n"
                + "ELSE abs.status_absensi\n"
                + "END as status_absensi,\n"
                + "CASE WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 6 THEN 'Hari Minggu'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND WEEKDAY(c.tanggal) = 5 THEN 'Hari Sabtu'\n"
                + "WHEN (asn = '-' OR asn IS NULL) AND libur.holiday_date IS NOT NULL THEN libur.holiday_note\n"
                //       + "WHEN (asn = '-' OR asn IS NULL) AND ex.alasan IS NOT NULL THEN ex.alasan\n"
                + "ELSE abs.keterangan\n"
                + "END as ket\n"
                + "FROM(\n"
                + "select selected_date as tanggal from\n"
                + "(select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1)v\n"
                + "where selected_date between '" + awal + "' and '" + akhir + "') c\n"
                + "LEFT JOIN tvri_pegawai_data_absensi abs ON abs.tanggal = c.tanggal AND abs.id_pegawai = '" + id + "'\n"
                + "LEFT JOIN holiday libur ON libur.holiday_date = c.tanggal\n"
                //+ "LEFT JOIN ex_absent ex ON CAST(ex.tgl_awal as date) = c.tanggal AND ex.emp_id_auto = abs.id_pegawai\n"
                + "ORDER BY c.tanggal";

        komp.setDataTabelCek(kon, tabelDetail1, sql, 1, 2);
    }

    private void cariData() {
        btnAbsensi.setEnabled(false);
        btnPribadi.setEnabled(false);
        btnCetak.setEnabled(false);
        cetakSelect.setEnabled(false);
        String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
        String nama = filter_nama.getText().trim();
        String unitku = ((Item) unit.getSelectedItem()).getValue().toString();
        if (unitku.equalsIgnoreCase("0")) {
            unitku = "";
        } else {
            unitku = " AND e.cab_id_auto = '" + unitku + "'";
        }

        String sql = "SELECT 'false' as cek, v.*, (hari - (hadir+sakit+cuti+diklat+dinas)) as bolos FROM (\n"
                + "SELECT e.emp_id_auto, CONCAT(e.first_name, ' ', e.last_name) as nama, e.nik,\n"
                + "IFNULL(pd.gol,'-') as gol,\n"
                + "CASE WHEN pd.pegawai IS NULL THEN '-' WHEN pd.pegawai = 'NON PNS' THEN 'BUKAN PNS' ELSE pd.pegawai END as st,\n"
                + "IFNULL(pd.norek,'-') as rek,\n"
                + "(\n"
                + "select SUM(CASE WHEN WEEKDAY(selected_date) IN (5, 6) OR libur.holiday_date IS NOT NULL THEN 0 ELSE 1 END) as tanggal from \n"
                + "                    (select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                + "                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                + "                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1) v\n"
                + "                     LEFT JOIN holiday libur ON libur.holiday_date = selected_date\n"
                + "                     where selected_date between '" + awal + "' and '" + akhir + "'\n"
                + ") as hari,\n"
                + "SUM(CASE WHEN t.keterangan LIKE '%Masuk Terlambat%' OR t.keterangan LIKE '%Tidak Absen Masuk%' THEN 1 ELSE 0 END) as terlambat,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Absen 1 Kali' THEN 1 ELSE 0 END) as absen_satu,\n"
                + "SUM(CASE WHEN t.status_absensi LIKE 'Sakit%' THEN 1 ELSE 0 END) as sakit,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Ijin' THEN 1 ELSE 0 END) as ijin,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Cuti' THEN 1 ELSE 0 END) as cuti,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Diklat' THEN 1 ELSE 0 END) as diklat,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Dinas' THEN 1 ELSE 0 END) as dinas,\n"
                + "SUM(CASE WHEN t.status_absensi = 'Masuk' THEN 1 ELSE 0 END) as hadir\n"
                + "\n"
                + "FROM emp e\n"
                + "LEFT JOIN tvri_pegawai_data_absensi t ON t.id_pegawai = e.emp_id_auto AND t.tanggal BETWEEN '" + awal + "' and '" + akhir + "'\n"
                + "LEFT JOIN ex_absent b ON CAST(b.tgl_awal as date) = t.tanggal AND b.emp_id_auto = e.emp_id_auto\n"
                + "LEFT JOIN cabang c ON c.cab_id_auto = e.cab_id_auto\n"
                + "LEFT JOIN tvri_pegawai_detail pd ON pd.emp_id_auto = e.emp_id_auto\n"
                + "WHERE e.emp_status = '0' AND (CONCAT(e.first_name, '', e.last_name) LIKE '%" + nama + "%' OR pd.pegawai LIKE '%" + nama + "%')" + unitku
                + " GROUP BY CONCAT(e.first_name, '', e.last_name), e.nik\n"
                + ") v ORDER BY v.nama";

        komp.setDataTabelCek(kon, tabelData, sql, 2, 1);
        if (tabelData.getRowCount() > 0) {
            btnCetak.setEnabled(true);
        }
    }

    private void hapusPresensi() {
        String id = preId.getText();

        String idPeg = detId.getText();

        String ins = "DELETE FROM tvri_pegawai_data_absensi WHERE id = '" + id + "'";

        boolean ok = komp.setSQL(kon, ins);
        if (ok) {
            pesan.pesanSukses("Sukses Hapus Data", "Data Presensi Sukses dihapus");
            detailPresensi(idPeg);
        } else {
            pesan.pesanError("Gagal Hapus Data", ins, "Hubungi Administrator");
        }
    }

    private void updatePresensiAll() {
        String idPeg = detId1.getText();
        String sta = preStatus1.getSelectedItem().toString();
        String ket = preKet1.getText();
        int y = tabelDetail1.getRowCount();

        for (int x = 0; x < y; x++) {
            String cek = tabelDetail1.getValueAt(x, 2).toString();
            String tgl = tabelDetail1.getValueAt(x, 4).toString();
            String id = tabelDetail1.getValueAt(x, 1).toString();
            if (cek.equalsIgnoreCase("true")) {
                String ins = "UPDATE tvri_pegawai_data_absensi SET keterangan = '" + ket + "', status_absensi = '" + sta + "' WHERE id = '" + id + "'";
                if (id.equalsIgnoreCase("KOSONG")) {
                    ins = "INSERT INTO tvri_pegawai_data_absensi"
                            + "(id_pegawai, no_absen, tanggal, keterangan, status_absensi, sort_tanggal, from_date) "
                            + "VALUES('" + idPeg + "','" + idPeg + "','" + tgl + "','" + ket + "','" + sta + "','" + tgl.replaceAll("-", "") + "',now())";

                }

                boolean ok = komp.setSQL(kon, ins);
            }
            //   System.out.println(cek);
        }
        detailPresensi1(idPeg);
    }

    private void updatePresensi() {
        String id = preId.getText();
        String tgl = preTanggal.getText();
        String idPeg = detId.getText();
        String sta = preStatus.getSelectedItem().toString();
        String ket = preKet.getText();
        String ins = "UPDATE tvri_pegawai_data_absensi SET keterangan = '" + ket + "', status_absensi = '" + sta + "' WHERE id = '" + id + "'";

        if (id.equalsIgnoreCase("KOSONG")) {
            ins = "INSERT INTO tvri_pegawai_data_absensi"
                    + "(id_pegawai, no_absen, tanggal, keterangan, status_absensi, sort_tanggal, from_date) "
                    + "VALUES('" + idPeg + "','" + idPeg + "','" + tgl + "','" + ket + "','" + sta + "','" + tgl.replaceAll("-", "") + "',now())";

        }

        boolean ok = komp.setSQL(kon, ins);
        if (ok) {
            pesan.pesanSukses("Sukses Update Data", "Data Presensi Sukses diupdate");
            detailPresensi(idPeg);
        } else {
            pesan.pesanError("Gagal Update Data", ins, "Hubungi Administrator");
        }

    }

    private void simpanPegawai() {
        String id = pId.getText();
        String nama = pNama.getText();
        String nik = pNik.getText();
        String gol = pGol.getSelectedItem().toString();
        String st = pStatus.getSelectedItem().toString();
        String rek = pNorek.getText();

        if (gol.equalsIgnoreCase("-- Pilih Pangkat/Golongan --")) {
            gol = "-";
        }

        if (st.equalsIgnoreCase("-")) {
            st = "-";
        } else if (st.equalsIgnoreCase("BUKAN PNS")) {
            st = "NON PNS";
        }

        String upEmp = "UPDATE emp SET nik = '" + nik + "' WHERE emp_id_auto = '" + id + "'";
        String inDet = "INSERT INTO tvri_pegawai_detail(emp_id_auto, gol, norek, pegawai) "
                + "VALUES ('" + id + "','" + gol + "','" + rek + "','" + st + "')";
        String upDet = "UPDATE tvri_pegawai_detail SET gol = '" + gol + "', norek = '" + rek + "', pegawai = '" + st + "' WHERE emp_id_auto = '" + id + "'";

        komp.setSQL(kon, upEmp);
        boolean ok = komp.setSQL(kon, inDet);
        if (!ok) {
            komp.setSQL(kon, upDet);
        }
    }
    int rowCek = -1;
    int totRow = 0;

    private int cek() {
        int x = 0;
        int yy = tabelData.getRowCount();
        totRow = yy;
        for (int hh = 0; hh < yy; hh++) {
            String xy = tabelData.getValueAt(hh, 1).toString();
            if (xy.equalsIgnoreCase("true")) {
                x++;
                if (x == 1) {
                    rowCek = hh;
                } else {
                    rowCek = -1;
                }
            }
            // System.out.println(xy);
        }

        return x;
    }

    private void cetakLaporan(String in) {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();
        String sql = "SELECT e.emp_id_auto, CONCAT(first_name,' ', last_name) as nama, f.func_name, d.cab_name, nik, IFNULL(tv.nilai, '-') as gol FROM emp e LEFT JOIN cabang d ON d.cab_id_auto = e.cab_id_auto LEFT JOIN func f ON f.func_id_auto = e.func_id_auto LEFT JOIN dept g ON g.dept_id_auto = e.dept_id_auto LEFT JOIN tvri_pegawai_detail xh ON xh.emp_id_auto = e.emp_id_auto LEFT JOIN tvri_list tv ON tv.kode = xh.gol WHERE e.emp_id_auto IN " + in;
        //     System.out.println(sql);
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            JasperPrint page1 = null;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
                String e = rset.getString(5);
                String f = rset.getString(6);
                if (rset.isFirst()) {
                    page1 = cetakLoopDetailReport(a, b, c, d, e, f, nama, nip, jabatan);
                } else {
                    JasperPrint page2 = cetakLoopDetailReport(a, b, c, d, e, f, nama, nip, jabatan);
                    page1 = multipageLinking(page1, page2);
                }
            }
            rset.close();
            stat.close();
            // kon.close();

            //   JasperViewer viewer = new JasperViewer(page1);
            //   Container container = viewer.getContentPane();
            JasperViewer.viewReport(page1, false);
        } catch (SQLException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JasperPrint multipageLinking(JasperPrint page1, JasperPrint page2) {
        List<JRPrintPage> pages = page2.getPages();
        for (int count = 0; count
                < pages.size(); count++) {
            page1.addPage(pages.get(count));
        }

        return page1;
    }

    private JasperPrint cetakLoopDetailReport(String id_peg, String nama, String jabatan, String devisi,
            String nip, String gol, String aNama, String aNip, String aJab) {
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
            Hashtable hparam = new Hashtable(1);
            hparam.put("id_peg", id_peg);
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("nama", nama);
            hparam.put("jabatan", jabatan);
            hparam.put("devisi", devisi);
            hparam.put("gol", gol);
            hparam.put("nip", nip);
            hparam.put("nip_kepala", aNip);
            hparam.put("nama_kepala", aNama);
            hparam.put("jabatan_kepala", aJab);
            String source = folder + "detailPresensi.jasper";
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
            //     JasperViewer.viewReport(JPrint, false);

        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pribadi = new javax.swing.JDialog();
        jPanel8 = new javax.swing.JPanel();
        pNama = new javax.swing.JTextField();
        pNik = new javax.swing.JTextField();
        pNorek = new javax.swing.JTextField();
        pStatus = new javax.swing.JComboBox<>();
        pGol = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        pId = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        presensi = new javax.swing.JDialog();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelDetail = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        detNik = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        detNama = new javax.swing.JLabel();
        panelDetailPresensi = new javax.swing.JPanel();
        preStatus = new javax.swing.JComboBox<>();
        preTanggal = new javax.swing.JTextField();
        preId = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        preKet = new javax.swing.JTextArea();
        preHari = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jml = new javax.swing.JLabel();
        detId = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jButton10 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        popMenu = new javax.swing.JPopupMenu();
        cetakSelect = new javax.swing.JMenuItem();
        cetakAll = new javax.swing.JMenuItem();
        presensi1 = new javax.swing.JDialog();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelDetail1 = new javax.swing.JTable();
        jLabel18 = new javax.swing.JLabel();
        detNik1 = new javax.swing.JLabel();
        detId1 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        detNama1 = new javax.swing.JLabel();
        panelDetailPresensi1 = new javax.swing.JPanel();
        preStatus1 = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        preKet1 = new javax.swing.JTextArea();
        jButton7 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        xxyy = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        btnCetak = new javax.swing.JButton();
        btnAbsensi = new javax.swing.JButton();
        btnPribadi = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        periode_bulan = new com.toedter.calendar.JMonthChooser();
        periode_tahun = new com.toedter.calendar.JYearChooser();
        jLabel3 = new javax.swing.JLabel();
        tanggal_awal = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        tanggal_akhir = new com.toedter.calendar.JDateChooser();
        labelUnit = new javax.swing.JLabel();
        unit = new javax.swing.JComboBox<>();
        labelUnit1 = new javax.swing.JLabel();
        filter_nama = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pNama.setEditable(false);
        jPanel8.add(pNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 310, -1));
        jPanel8.add(pNik, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 60, 310, -1));
        jPanel8.add(pNorek, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 310, -1));

        pStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pStatusItemStateChanged(evt);
            }
        });
        jPanel8.add(pStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 110, -1));

        pGol.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel8.add(pGol, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 200, -1));

        jLabel6.setText("Nomor Rekening");
        jPanel8.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 110, 20));

        jLabel7.setText("Nama");
        jPanel8.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 110, 20));

        jLabel8.setText("NIP / NIK");
        jPanel8.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 110, 20));

        jLabel9.setText("Status Pegawai");
        jPanel8.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 110, 20));

        jLabel10.setText("Pangkat/Golongan");
        jPanel8.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 110, 20));

        pId.setEditable(false);
        jPanel8.add(pId, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 30, 50, -1));

        pribadi.getContentPane().add(jPanel8, java.awt.BorderLayout.CENTER);

        jPanel9.setBackground(new java.awt.Color(153, 255, 153));
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("Data Pegawai");
        jLabel5.setToolTipText("");
        jPanel9.add(jLabel5);

        pribadi.getContentPane().add(jPanel9, java.awt.BorderLayout.PAGE_START);

        jPanel10.setBackground(new java.awt.Color(255, 153, 153));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton2.setText("Simpan");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel10.add(jButton2);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton3.setText("Tutup");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel10.add(jButton3);

        pribadi.getContentPane().add(jPanel10, java.awt.BorderLayout.PAGE_END);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        pribadi.getContentPane().add(jPanel11, java.awt.BorderLayout.LINE_END);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        pribadi.getContentPane().add(jPanel12, java.awt.BorderLayout.LINE_START);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabelDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Hari", "Tanggal", "Masuk", "Pulang", "Durasi", "Status", "Keterangan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelDetail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDetailMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabelDetail);

        jPanel13.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 510, 530));

        jLabel12.setText("NIP/NIK");
        jPanel13.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 80, -1));

        detNik.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detNik.setText("Nama");
        jPanel13.add(detNik, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 300, -1));

        jLabel14.setText("Nama");
        jPanel13.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 80, -1));

        detNama.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detNama.setText("Nama");
        jPanel13.add(detNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 300, -1));

        panelDetailPresensi.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), " Status Presensi "));
        panelDetailPresensi.setOpaque(false);
        panelDetailPresensi.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        preStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        panelDetailPresensi.add(preStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 210, -1));

        preTanggal.setEditable(false);
        panelDetailPresensi.add(preTanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 80, -1));

        preId.setEditable(false);
        panelDetailPresensi.add(preId, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 30, 30, -1));

        preKet.setColumns(20);
        preKet.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        preKet.setLineWrap(true);
        preKet.setRows(5);
        jScrollPane3.setViewportView(preKet);

        panelDetailPresensi.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 210, 110));

        preHari.setEditable(false);
        panelDetailPresensi.add(preHari, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 80, -1));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton6.setText("Update Data");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        panelDetailPresensi.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 250, -1, -1));

        jLabel13.setText("Keterangan");
        panelDetailPresensi.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 80, 20));

        jLabel15.setText("Hari");
        panelDetailPresensi.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 80, 20));

        jLabel16.setText("Tanggal");
        panelDetailPresensi.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 80, 20));

        jLabel17.setText("Status");
        panelDetailPresensi.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 80, 20));

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton4.setText("Hapus");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        panelDetailPresensi.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 250, 90, -1));

        jPanel13.add(panelDetailPresensi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 80, 360, 300));

        jml.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jml.setText("000");
        jPanel13.add(jml, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 20, 40, -1));

        detId.setEditable(false);
        jPanel13.add(detId, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 400, 30, -1));

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("/");
        jPanel13.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(455, 20, 10, -1));

        total.setText("000");
        jPanel13.add(total, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 20, 30, -1));

        presensi.getContentPane().add(jPanel13, java.awt.BorderLayout.CENTER);

        jPanel14.setBackground(new java.awt.Color(153, 153, 255));
        jPanel14.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 20));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("DETAIL PRESENSI PEGAWAI");
        jPanel14.add(jLabel11);

        presensi.getContentPane().add(jPanel14, java.awt.BorderLayout.PAGE_START);

        jPanel15.setBackground(new java.awt.Color(255, 153, 153));

        jButton10.setText("<< Back");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton10);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton5.setText("Tutup");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton5);

        jButton8.setText("Next >>");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton8);

        presensi.getContentPane().add(jPanel15, java.awt.BorderLayout.PAGE_END);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        presensi.getContentPane().add(jPanel16, java.awt.BorderLayout.LINE_END);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        presensi.getContentPane().add(jPanel17, java.awt.BorderLayout.LINE_START);

        cetakSelect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/customer.png"))); // NOI18N
        cetakSelect.setText("Cetak Terpilih");
        cetakSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakSelectActionPerformed(evt);
            }
        });
        popMenu.add(cetakSelect);

        cetakAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/supplier_menu.png"))); // NOI18N
        cetakAll.setText("Cetak Semua");
        cetakAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakAllActionPerformed(evt);
            }
        });
        popMenu.add(cetakAll);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabelDetail1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Cek", "Hari", "Tanggal", "Masuk", "Pulang", "Durasi", "Status", "Keterangan"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelDetail1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDetail1MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabelDetail1);

        jPanel18.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 510, 530));

        jLabel18.setText("NIP/NIK");
        jPanel18.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 80, -1));

        detNik1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detNik1.setText("Nama");
        jPanel18.add(detNik1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 300, -1));

        detId1.setEditable(false);
        jPanel18.add(detId1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 40, -1));

        jLabel19.setText("Nama");
        jPanel18.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 80, -1));

        detNama1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detNama1.setText("Nama");
        jPanel18.add(detNama1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 300, -1));

        panelDetailPresensi1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), " Status Presensi "));
        panelDetailPresensi1.setOpaque(false);
        panelDetailPresensi1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        preStatus1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        panelDetailPresensi1.add(preStatus1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 210, -1));

        preKet1.setColumns(20);
        preKet1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        preKet1.setLineWrap(true);
        preKet1.setRows(5);
        jScrollPane5.setViewportView(preKet1);

        panelDetailPresensi1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 210, 110));

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton7.setText("Update Data");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        panelDetailPresensi1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, -1, -1));

        jLabel20.setText("Keterangan");
        panelDetailPresensi1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 80, 20));

        jLabel23.setText("Status");
        panelDetailPresensi1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 80, 20));

        jPanel18.add(panelDetailPresensi1, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 80, 360, 240));

        presensi1.getContentPane().add(jPanel18, java.awt.BorderLayout.CENTER);

        jPanel19.setBackground(new java.awt.Color(153, 153, 255));
        jPanel19.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 20));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel24.setText("DETAIL PRESENSI PEGAWAI");
        jPanel19.add(jLabel24);

        presensi1.getContentPane().add(jPanel19, java.awt.BorderLayout.PAGE_START);

        jPanel20.setBackground(new java.awt.Color(255, 153, 153));

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton9.setText("Tutup");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel20.add(jButton9);

        presensi1.getContentPane().add(jPanel20, java.awt.BorderLayout.PAGE_END);

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));
        presensi1.getContentPane().add(jPanel21, java.awt.BorderLayout.LINE_END);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));
        presensi1.getContentPane().add(jPanel22, java.awt.BorderLayout.LINE_START);

        setTitle(".:: Presensi Pegawai ::.");
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        xxyy.setBackground(new java.awt.Color(255, 255, 255));
        xxyy.setLayout(new java.awt.BorderLayout());

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel7.setOpaque(false);
        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 10));

        btnCetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/report.png"))); // NOI18N
        btnCetak.setText("Cetak Detail Presensi");
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });
        jPanel7.add(btnCetak);

        btnAbsensi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/option.png"))); // NOI18N
        btnAbsensi.setText("Data Presensi");
        btnAbsensi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbsensiActionPerformed(evt);
            }
        });
        jPanel7.add(btnAbsensi);

        btnPribadi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user.png"))); // NOI18N
        btnPribadi.setText("Data Pribadi");
        btnPribadi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPribadiActionPerformed(evt);
            }
        });
        jPanel7.add(btnPribadi);

        jPanel1.add(jPanel7, java.awt.BorderLayout.PAGE_START);

        jPanel6.setOpaque(false);
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Cek", "ID", "Nama", "NIK", "Golongan", "Status", "No Rekening", "Hari Kerja", "Terlambat", "Absen 1 kali", "Sakit", "Ijin", "Cuti", "Diklat", "Dinas", "Masuk", "Tidak Masuk"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDataMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelData);

        jPanel6.add(jScrollPane1);

        jPanel1.add(jPanel6, java.awt.BorderLayout.CENTER);

        xxyy.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 50, 170));

        jLabel2.setText("Periode");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, 20));
        jPanel2.add(periode_bulan, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, -1, -1));
        jPanel2.add(periode_tahun, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, -1, -1));

        jLabel3.setText("Tanggal Presensi");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, 20));

        tanggal_awal.setDate(new java.util.Date());
        tanggal_awal.setDateFormatString("dd/MM/yyyy");
        jPanel2.add(tanggal_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, -1, -1));

        jLabel4.setText("s/d");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 40, -1, 20));

        tanggal_akhir.setDate(new java.util.Date());
        tanggal_akhir.setDateFormatString("dd/MM/yyyy");
        jPanel2.add(tanggal_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, -1, -1));

        labelUnit.setText("Filter Nama");
        jPanel2.add(labelUnit, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 60, 20));

        unit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Rekap Presensi", "Detail Presensi" }));
        jPanel2.add(unit, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 290, -1));

        labelUnit1.setText("Unit Kerja");
        jPanel2.add(labelUnit1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, 20));

        filter_nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filter_namaKeyReleased(evt);
            }
        });
        jPanel2.add(filter_nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 100, 290, -1));

        jButton1.setText("Tampil Data");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 130, -1, -1));

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("X");
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 110, 70, 50));

        xxyy.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        xxyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setOpaque(false);
        xxyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        xxyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        getContentPane().add(xxyy);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cariData();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tabelDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDataMouseClicked
        int c = cek();
        if (c == 1) {
            btnAbsensi.setEnabled(true);
            btnPribadi.setEnabled(true);
        } else {
            btnAbsensi.setEnabled(false);
            btnPribadi.setEnabled(false);
        }
        if (c == 0) {
            cetakSelect.setEnabled(false);
        } else {
            cetakSelect.setEnabled(true);
        }
    }//GEN-LAST:event_tabelDataMouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        komp.closeDialogSukses(pribadi);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnAbsensiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbsensiActionPerformed
        if (rowCek >= 0) {
            String id = tabelData.getValueAt(rowCek, 2).toString();
            String nama = tabelData.getValueAt(rowCek, 3).toString();
            String nik = tabelData.getValueAt(rowCek, 4).toString();
            jml.setText(String.valueOf(rowCek + 1));
            total.setText(String.valueOf(totRow));
            //     String gol = tabelData.getValueAt(rowCek, 5).toString();
            //     String st = tabelData.getValueAt(rowCek, 6).toString();
            //     String rek = tabelData.getValueAt(rowCek, 7).toString();
            detNama.setText(nama);
            detNik.setText(nik);
            detailPresensi(id);

            komp.showDialog(presensi, 950, 780);
        }


    }//GEN-LAST:event_btnAbsensiActionPerformed

    private void pStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_pStatusItemStateChanged
        try {
            String a = pStatus.getSelectedItem().toString();
            if (a.equalsIgnoreCase("PNS")) {
                pGol.setEnabled(true);
            } else {
                pGol.setSelectedIndex(0);
                pGol.setEnabled(false);
            }
        } catch (Exception e) {
            pGol.setSelectedIndex(0);
            pGol.setEnabled(false);
        }
    }//GEN-LAST:event_pStatusItemStateChanged

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        simpanPegawai();
        cariData();
        komp.closeDialogSukses(pribadi);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnPribadiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPribadiActionPerformed
        if (rowCek >= 0) {
            String id = tabelData.getValueAt(rowCek, 2).toString();
            String nama = tabelData.getValueAt(rowCek, 3).toString();
            String nik = tabelData.getValueAt(rowCek, 4).toString();
            String gol = tabelData.getValueAt(rowCek, 5).toString();
            String st = tabelData.getValueAt(rowCek, 6).toString();
            String rek = tabelData.getValueAt(rowCek, 7).toString();

            pId.setText(id);
            pNama.setText(nama);
            pNik.setText(nik);
            pNorek.setText(rek);
            if (gol.equalsIgnoreCase("-")) {
                gol = "-- Pilih Pangkat/Golongan --";
            }
            pGol.setSelectedItem(gol);
            if (st.equalsIgnoreCase("-")) {
                st = "-- Pilih Status --";
            }
            pStatus.setSelectedItem(st);
            komp.showDialog(pribadi, 500, 350);
        }

    }//GEN-LAST:event_btnPribadiActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        komp.closeDialogSukses(presensi);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void filter_namaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filter_namaKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cariData();
        }
    }//GEN-LAST:event_filter_namaKeyReleased

    private void tabelDetailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDetailMouseClicked
        int yy = tabelDetail.getSelectedRow();
        if (yy >= 0) {
            String id = tabelDetail.getValueAt(yy, 1).toString();
            String hari = tabelDetail.getValueAt(yy, 2).toString();
            String tgl = tabelDetail.getValueAt(yy, 3).toString();
            String stat = tabelDetail.getValueAt(yy, 7).toString();
            String ket = tabelDetail.getValueAt(yy, 8).toString();
            preId.setVisible(false);
            preHari.setText(hari);
            preId.setText(id);
            preTanggal.setText(tgl);
            preStatus.setSelectedItem(stat);
            preKet.setText(ket);

            panelDetailPresensi.setVisible(true);
        } else {
            panelDetailPresensi.setVisible(false);
        }
    }//GEN-LAST:event_tabelDetailMouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        updatePresensi();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
        popMenu.show(btnCetak, 0, 25);
    }//GEN-LAST:event_btnCetakActionPerformed

    private void cetakSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakSelectActionPerformed
        int yy = tabelData.getRowCount();
        String in = "(";
        for (int hh = 0; hh < yy; hh++) {
            String bh = tabelData.getValueAt(hh, 1).toString();
            if (bh.equalsIgnoreCase("true")) {
                String xy = tabelData.getValueAt(hh, 2).toString();
                in = in + "'" + xy + "',";
            }
        }
        in = in.substring(0, in.length() - 2) + "')";
        cetakLaporan(in);

    }//GEN-LAST:event_cetakSelectActionPerformed

    private void cetakAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakAllActionPerformed
        int yy = tabelData.getRowCount();
        String in = "(";
        for (int hh = 0; hh < yy; hh++) {
            //  String xy = tabelData.getValueAt(hh, 1).toString();
            String xy = tabelData.getValueAt(hh, 2).toString();
            in = in + "'" + xy + "',";
        }
        in = in.substring(0, in.length() - 2) + "')";
        cetakLaporan(in);
    }//GEN-LAST:event_cetakAllActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        int y = JOptionPane.showConfirmDialog(presensi, "Anda yakin ingin menghapus data? jika yakin silahkan tekan YES", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            hapusPresensi();
        }

    }//GEN-LAST:event_jButton4ActionPerformed

    private void tabelDetail1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDetail1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tabelDetail1MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        updatePresensiAll();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked

        String iw = JOptionPane.showInternalInputDialog(this, "Masukkan Kata");
        if (iw.equalsIgnoreCase("iweksganteng")) {
            if (rowCek >= 0) {
                String id = tabelData.getValueAt(rowCek, 2).toString();
                String nama = tabelData.getValueAt(rowCek, 3).toString();
                String nik = tabelData.getValueAt(rowCek, 4).toString();
                //     String gol = tabelData.getValueAt(rowCek, 5).toString();
                //     String st = tabelData.getValueAt(rowCek, 6).toString();
                //     String rek = tabelData.getValueAt(rowCek, 7).toString();
                detNama1.setText(nama);
                detNik1.setText(nik);
                detailPresensi1(id);

                komp.showDialog(presensi1, 950, 780);
            }
        }
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        int tot = totRow;
        int row = Integer.parseInt(jml.getText()) + 1;
        if (row > tot) {
            row = 1;
        }
        jml.setText(String.valueOf(row));
        String id = tabelData.getValueAt(row - 1, 2).toString();
        String nama = tabelData.getValueAt(row - 1, 3).toString();
        String nik = tabelData.getValueAt(row - 1, 4).toString();

        detNama.setText(nama);
        detNik.setText(nik);
        detailPresensi(id);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        int tot = totRow;
        int row = Integer.parseInt(jml.getText()) - 1;
        if (row < 1) {
            row = tot;
        }
        jml.setText(String.valueOf(row));
        String id = tabelData.getValueAt(row - 1, 2).toString();
        String nama = tabelData.getValueAt(row - 1, 3).toString();
        String nik = tabelData.getValueAt(row - 1, 4).toString();

        detNama.setText(nama);
        detNik.setText(nik);
        detailPresensi(id);
    }//GEN-LAST:event_jButton10ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbsensi;
    private javax.swing.JButton btnCetak;
    private javax.swing.JButton btnPribadi;
    private javax.swing.JMenuItem cetakAll;
    private javax.swing.JMenuItem cetakSelect;
    private javax.swing.JTextField detId;
    private javax.swing.JTextField detId1;
    private javax.swing.JLabel detNama;
    private javax.swing.JLabel detNama1;
    private javax.swing.JLabel detNik;
    private javax.swing.JLabel detNik1;
    private javax.swing.JTextField filter_nama;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel jml;
    private javax.swing.JLabel labelUnit;
    private javax.swing.JLabel labelUnit1;
    private javax.swing.JComboBox<String> pGol;
    private javax.swing.JTextField pId;
    private javax.swing.JTextField pNama;
    private javax.swing.JTextField pNik;
    private javax.swing.JTextField pNorek;
    private javax.swing.JComboBox<String> pStatus;
    private javax.swing.JPanel panelDetailPresensi;
    private javax.swing.JPanel panelDetailPresensi1;
    private com.toedter.calendar.JMonthChooser periode_bulan;
    private com.toedter.calendar.JYearChooser periode_tahun;
    private javax.swing.JPopupMenu popMenu;
    private javax.swing.JTextField preHari;
    private javax.swing.JTextField preId;
    private javax.swing.JTextArea preKet;
    private javax.swing.JTextArea preKet1;
    private javax.swing.JComboBox<String> preStatus;
    private javax.swing.JComboBox<String> preStatus1;
    private javax.swing.JTextField preTanggal;
    private javax.swing.JDialog presensi;
    private javax.swing.JDialog presensi1;
    private javax.swing.JDialog pribadi;
    private javax.swing.JTable tabelData;
    private javax.swing.JTable tabelDetail;
    private javax.swing.JTable tabelDetail1;
    private com.toedter.calendar.JDateChooser tanggal_akhir;
    private com.toedter.calendar.JDateChooser tanggal_awal;
    private javax.swing.JLabel total;
    private javax.swing.JComboBox<String> unit;
    private javax.swing.JPanel xxyy;
    // End of variables declaration//GEN-END:variables
}
