/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absensi;

import java.awt.Container;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import utility.Item;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class laporanAbsensi extends javax.swing.JInternalFrame {

    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    String user = "";
    Connection kon;

    /**
     * Creates new form laporanAbsensi
     */
    public laporanAbsensi(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        komp.setJComboBoxListVektor(unit, "SELECT cab_id_auto, cab_name FROM cabang WHERE cab_name != 'Kantor Pusat' ORDER BY cab_name", "-- SEMUA UNIT KERJA --", kon);
        tipe.setVisible(false);

    }

    private void showReport() {
        int p = jenis.getSelectedIndex();
        if (p == 0 || p == 3) {
            String unitku = ((Item) unit.getSelectedItem()).getValue().toString();
            String nama = ((Item) unit.getSelectedItem()).getDescription().toString();
            if (unitku.equalsIgnoreCase("0")) {
                rekapReportSemuaUnit();

            } else {
                //   reportRekap(unitku, nama);
                JasperPrint page1 = reportRekap(unitku, nama);
                JasperViewer viewer = new JasperViewer(page1);
                Container container = viewer.getContentPane();
                data.removeAll();
                data.add(container);
                data.repaint();
            }

        } else if (p == 4) {
            reportDetail();
        } else if (p == 7) {
            reportDetailSkip();
        } else if (p == 5) {
            reportKuitansi();
        } else if (p == 6) {
            JasperPrint page1 = reportRekapAll();
            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();

        } else if (p == 2) {
            reportUangMakan();
        } else if (p == 1) {
            JasperPrint page1 = reportRekapPejabat();
            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        }
    }

    private JasperPrint multipageLinking(JasperPrint page1, JasperPrint page2) {
        List<JRPrintPage> pages = page2.getPages();
        for (int count = 0; count
                < pages.size(); count++) {
            page1.addPage(pages.get(count));
        }

        return page1;
    }

    private void reportUangMakan() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();

        Object[] jk1 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'KEUANGAN'");
        String nama1 = jk1[0].toString();
        String nip1 = jk1[1].toString();
        String jabatan1 = jk1[2].toString();

        Object[] jk2 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'BENDAHARA'");
        String nama2 = jk2[0].toString();
        String nip2 = jk2[1].toString();
        String jabatan2 = jk2[2].toString();

        Object[] jk3 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'KEPALA'");
        String nama3 = jk3[0].toString();
        String nip3 = jk3[1].toString();
        String jabatan3 = jk3[2].toString();
        Object[] jk4 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'STAF'");
        String nama4 = jk4[0].toString();
        String nip4 = jk4[1].toString();
        String jabatan4 = jk4[2].toString();
        try {
            String tp = tipe.getSelectedItem().toString();
            String nm = "PEGAWAI NEGERI SIPIL";

            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
            Hashtable hparam = new Hashtable(1);

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);

            hparam.put("judul", nm);

            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            String source = folder + "uangMakan.jasper";
            if (tp.equalsIgnoreCase("NON PNS") || tp.equalsIgnoreCase("BUKAN PNS")) {
                tp = "NON PNS";
                nm = "PEGAWAI BUKAN PEGAWAI NEGERI SIPIL";
                source = folder + "uangMakanNon.jasper";
                hparam.put("nip_kepala", nip);
                hparam.put("nama_kepala", nama);
                hparam.put("jabatan_kepala", jabatan);
                hparam.put("nip_kepala_1", nip1);
                hparam.put("nama_kepala_1", nama1);
                hparam.put("jabatan_kepala_1", jabatan1);
            } else {
                hparam.put("nip_kepala", nip4);
                hparam.put("nama_kepala", nama4);
                hparam.put("jabatan_kepala", jabatan4);
                hparam.put("nip_kepala_1", nip3);
                hparam.put("nama_kepala_1", nama3);
                hparam.put("jabatan_kepala_1", jabatan3);
                hparam.put("nip_kepala_1_1", nip2);
                hparam.put("nama_kepala_1_1", nama2);
                hparam.put("jabatan_kepala_1_1", jabatan2);
            }
            hparam.put("tipe", tp);
            hparam.put("gambar", folder + "logo.png");
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JasperPrint JPrint = JasperFillManager.fillReport(report, hparam, kon);
            JasperViewer viewer = new JasperViewer(JPrint);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        } catch (JRException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reportKuitansi() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();

        Object[] jk1 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'KEUANGAN'");
        String nama1 = jk1[0].toString();
        String nip1 = jk1[1].toString();
        String jabatan1 = jk1[2].toString();

        Object[] jk2 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'BENDAHARA'");
        String nama2 = jk2[0].toString();
        String nip2 = jk2[1].toString();
        String jabatan2 = jk2[2].toString();

        Object[] jk3 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'KEPALA'");
        String nama3 = jk3[0].toString();
        String nip3 = jk3[1].toString();
        String jabatan3 = jk3[2].toString();
        Object[] jk4 = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'STAF'");
        String nama4 = jk4[0].toString();
        String nip4 = jk4[1].toString();
        String jabatan4 = jk4[2].toString();
        try {
            String tp = tipe.getSelectedItem().toString();
            String nm = "PEGAWAI NEGERI SIPIL";

            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
            Hashtable hparam = new Hashtable(1);

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);

            hparam.put("judul", nm);

            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            String source = folder + "uangMakanRekap.jasper";
            String source1 = folder + "uangMakanRekapKuitansiGolongan.jasper";
            String source2 = folder + "uangMakanRekapKuitansi.jasper";
            if (tp.equalsIgnoreCase("NON PNS") || tp.equalsIgnoreCase("BUKAN PNS")) {
                nm = "PEGAWAI BUKAN PEGAWAI NEGERI SIPIL";
                tp = "NON PNS";
                //   source = folder + "uangMakanNon.jasper";
                hparam.put("nip_kepala", nip);
                hparam.put("nama_kepala", nama);
                hparam.put("jabatan_kepala", jabatan);
                hparam.put("nip_kepala_1", nip1);
                hparam.put("nama_kepala_1", nama1);
                hparam.put("jabatan_kepala_1", jabatan1);
            } else {
                hparam.put("nip_kepala", nip4);
                hparam.put("nama_kepala", nama4);
                hparam.put("jabatan_kepala", jabatan4);
                hparam.put("nip_kepala_1", nip3);
                hparam.put("nama_kepala_1", nama3);
                hparam.put("jabatan_kepala_1", jabatan3);
                hparam.put("nip_kepala_1_1", nip2);
                hparam.put("nama_kepala_1_1", nama2);
                hparam.put("jabatan_kepala_1_1", jabatan2);
            }
            hparam.put("tipe", tp);
            hparam.put("gambar", folder + "logo.png");
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JasperReport report1 = (JasperReport) JRLoader.loadObject(source1);
            JasperReport report2 = (JasperReport) JRLoader.loadObject(source2);
            JasperPrint JPrint = JasperFillManager.fillReport(report, hparam, kon);
            JasperPrint kui1 = JasperFillManager.fillReport(report1, hparam, kon);
            JasperPrint kui2 = JasperFillManager.fillReport(report2, hparam, kon);
            JasperPrint page1 = multipageLinking(kui1, kui2);
            page1 = multipageLinking(page1, JPrint);
            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        } catch (JRException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void rekapReportSemuaUnit() {
        String sql = "SELECT cab_id_auto, cab_name FROM cabang WHERE cab_name != 'Kantor Pusat' ORDER BY cab_name";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            JasperPrint page1 = null;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);

                if (rset.isFirst()) {
                    page1 = reportRekap(a, b);
                } else {
                    JasperPrint page2 = reportRekap(a, b);
                    page1 = multipageLinking(page1, page2);
                }
            }
            rset.close();
            stat.close();
            // kon.close();

            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        } catch (SQLException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reportDetail() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();

        String unitku = ((Item) unit.getSelectedItem()).getValue().toString();
        String sql = "SELECT emp_id_auto, CONCAT(first_name,' ', last_name) as nama, f.func_name, d.cab_name, nik, education FROM emp e LEFT JOIN cabang d ON d.cab_id_auto = e.cab_id_auto LEFT JOIN func f ON f.func_id_auto = e.func_id_auto LEFT JOIN dept g ON g.dept_id_auto = e.dept_id_auto";
        if (!unitku.equalsIgnoreCase("0")) {
            sql = "SELECT emp_id_auto, CONCAT(first_name,' ', last_name) as nama, f.func_name, d.cab_name, nik, education FROM emp e LEFT JOIN cabang d ON d.cab_id_auto = e.cab_id_auto LEFT JOIN func f ON f.func_id_auto = e.func_id_auto LEFT JOIN dept g ON g.dept_id_auto = e.dept_id_auto WHERE d.cab_id_auto = '" + unitku + "'";
        }
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            JasperPrint page1 = null;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
                String e = rset.getString(5);
                String f = rset.getString(6);
                if (rset.isFirst()) {
                    page1 = cetakLoopDetailReport(a, b, c, d, e, f, nama, nip, jabatan);
                } else {
                    JasperPrint page2 = cetakLoopDetailReport(a, b, c, d, e, f, nama, nip, jabatan);
                    page1 = multipageLinking(page1, page2);
                }
            }
            rset.close();
            stat.close();
            // kon.close();

            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        } catch (SQLException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reportDetailSkip() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();

        String unitku = ((Item) unit.getSelectedItem()).getValue().toString();
        String sql = "SELECT emp_id_auto, CONCAT(first_name,' ', last_name) as nama, f.func_name, d.cab_name, nik, education FROM emp e LEFT JOIN cabang d ON d.cab_id_auto = e.cab_id_auto LEFT JOIN func f ON f.func_id_auto = e.func_id_auto LEFT JOIN dept g ON g.dept_id_auto = e.dept_id_auto";
        if (!unitku.equalsIgnoreCase("0")) {
            sql = "SELECT emp_id_auto, CONCAT(first_name,' ', last_name) as nama, f.func_name, d.cab_name, nik, education FROM emp e LEFT JOIN cabang d ON d.cab_id_auto = e.cab_id_auto LEFT JOIN func f ON f.func_id_auto = e.func_id_auto LEFT JOIN dept g ON g.dept_id_auto = e.dept_id_auto WHERE d.cab_id_auto = '" + unitku + "'";
        }
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            JasperPrint page1 = null;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
                String e = rset.getString(5);
                String f = rset.getString(6);
                if (rset.isFirst()) {
                    page1 = cetakLoopDetailReportSkip(a, b, c, d, e, f, nama, nip, jabatan);
                } else {
                    JasperPrint page2 = cetakLoopDetailReportSkip(a, b, c, d, e, f, nama, nip, jabatan);
                    page1 = multipageLinking(page1, page2);
                }
            }
            rset.close();
            stat.close();
            // kon.close();

            JasperViewer viewer = new JasperViewer(page1);
            Container container = viewer.getContentPane();
            data.removeAll();
            data.add(container);
            data.repaint();
        } catch (SQLException ex) {
            Logger.getLogger(laporanAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JasperPrint cetakLoopDetailReportSkip(String id_peg, String nama, String jabatan, String devisi,
            String nip, String gol, String aNama, String aNip, String aJab) {
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
            Hashtable hparam = new Hashtable(1);
            hparam.put("id_peg", id_peg);
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("nama", nama);
            hparam.put("jabatan", jabatan);
            hparam.put("devisi", devisi);
            hparam.put("gol", gol);
            hparam.put("nip", nip);
            hparam.put("nip_kepala", aNip);
            hparam.put("nama_kepala", aNama);
            hparam.put("jabatan_kepala", aJab);
            String source = folder + "detailPresensiSkip.jasper";
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
            //     JasperViewer.viewReport(JPrint, false);

        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    private JasperPrint cetakLoopDetailReport(String id_peg, String nama, String jabatan, String devisi,
            String nip, String gol, String aNama, String aNip, String aJab) {
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();
            Hashtable hparam = new Hashtable(1);
            hparam.put("id_peg", id_peg);
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("nama", nama);
            hparam.put("jabatan", jabatan);
            hparam.put("devisi", devisi);
            hparam.put("gol", gol);
            hparam.put("nip", nip);
            hparam.put("nip_kepala", aNip);
            hparam.put("nama_kepala", aNama);
            hparam.put("jabatan_kepala", aJab);
            String source = folder + "detailPresensi.jasper";
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
            //     JasperViewer.viewReport(JPrint, false);

        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    private JasperPrint reportRekap(String id_unit, String unit) {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);

            String source = folder + "rekapAbsensi.jasper";
            if (jenis.getSelectedIndex() == 3) {
                source = folder + "rekapAbsensiPengurangan.jasper";
            }
            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();

            String sql = "select SUM(CASE WHEN WEEKDAY(selected_date) IN (5, 6) OR libur.holiday_date IS NOT NULL THEN 0 ELSE 1 END) as tanggal from \n"
                    + "(select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1)v\n"
                    + "LEFT JOIN holiday libur ON libur.holiday_date = selected_date\n"
                    + "where selected_date between '" + awal + "' and '" + akhir + "'";
            int nm = komp.getIntSQL(kon, sql);

            Hashtable hparam = new Hashtable(1);
            //   hparam.put("tgl_dari", new java.sql.Date(tgl_dari.getDate().getTime()).toString());
            //   hparam.put("tgl_sampai", new java.sql.Date(tgl_sampai.getDate().getTime()).toString());
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("unit", unit);
            hparam.put("id_unit", id_unit);
            hparam.put("hari", nm);
            hparam.put("nip_kepala", nip);
            hparam.put("nama_kepala", nama);
            hparam.put("jabatan_kepala", jabatan);
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    private JasperPrint reportRekapPejabat() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'KEPALA'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);

            String source = folder + "rekapAbsensiStruktural.jasper";

            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();

            String sql = "select SUM(CASE WHEN WEEKDAY(selected_date) IN (5, 6) OR libur.holiday_date IS NOT NULL THEN 0 ELSE 1 END) as tanggal from \n"
                    + "(select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1)v\n"
                    + "LEFT JOIN holiday libur ON libur.holiday_date = selected_date\n"
                    + "where selected_date between '" + awal + "' and '" + akhir + "'";
            int nm = komp.getIntSQL(kon, sql);

            Hashtable hparam = new Hashtable(1);
            //   hparam.put("tgl_dari", new java.sql.Date(tgl_dari.getDate().getTime()).toString());
            //   hparam.put("tgl_sampai", new java.sql.Date(tgl_sampai.getDate().getTime()).toString());
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("hari", nm);
            hparam.put("nip_kepala", nip);
            hparam.put("nama_kepala", nama);
            hparam.put("jabatan_kepala", jabatan);
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    private JasperPrint reportRekapAll() {
        Object[] jk = komp.setDataEdit(kon, "SELECT nama, nip, jabatan FROM tvri_ttd WHERE kode = 'PRESENSI'");
        String nama = jk[0].toString();
        String nip = jk[1].toString();
        String jabatan = jk[2].toString();
        JasperPrint JPrint = null;
        try {
            String folder = System.getProperty("user.dir") + "\\Laporan\\";
            int t = periode_bulan.getMonth();
            int th = periode_tahun.getYear();
            String vv = komp.getBulan(t);

            String source = folder + "rekapAbsensiUnit.jasper";

            String awal = new java.sql.Date(tanggal_awal.getDate().getTime()).toString();
            String akhir = new java.sql.Date(tanggal_akhir.getDate().getTime()).toString();

            String sql = "select SUM(CASE WHEN WEEKDAY(selected_date) IN (5, 6) OR libur.holiday_date IS NOT NULL THEN 0 ELSE 1 END) as tanggal from \n"
                    + "(select adddate('" + awal + "', t1.i*10 + t0.i) selected_date from\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,\n"
                    + " (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1)v\n"
                    + "LEFT JOIN holiday libur ON libur.holiday_date = selected_date\n"
                    + "where selected_date between '" + awal + "' and '" + akhir + "'";
            int nm = komp.getIntSQL(kon, sql);

            Hashtable hparam = new Hashtable(1);
            //   hparam.put("tgl_dari", new java.sql.Date(tgl_dari.getDate().getTime()).toString());
            //   hparam.put("tgl_sampai", new java.sql.Date(tgl_sampai.getDate().getTime()).toString());
            hparam.put("gambar", folder + "logo.png");

            hparam.put("periode", vv.toUpperCase() + " " + String.valueOf(th));
            hparam.put("akhir", akhir);
            hparam.put("awal", awal);
            hparam.put("hari", nm);
            hparam.put("nip_kepala", nip);
            hparam.put("nama_kepala", nama);
            hparam.put("jabatan_kepala", jabatan);
            JasperReport report = (JasperReport) JRLoader.loadObject(source);
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        xxxyy = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jenis = new javax.swing.JComboBox<>();
        tipe = new javax.swing.JComboBox<>();
        labelUnit = new javax.swing.JLabel();
        unit = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        periode_bulan = new com.toedter.calendar.JMonthChooser();
        periode_tahun = new com.toedter.calendar.JYearChooser();
        jLabel3 = new javax.swing.JLabel();
        tanggal_awal = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        tanggal_akhir = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();

        setTitle("Laporan Absensi");
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        xxxyy.setLayout(new java.awt.BorderLayout());

        data.setLayout(new java.awt.GridLayout(1, 0));
        xxxyy.add(data, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.BorderLayout());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("LAPORAN PRESENSI PEGAWAI");
        jPanel3.add(jLabel5);

        jPanel1.add(jPanel3, java.awt.BorderLayout.NORTH);

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 15));

        jLabel1.setText("Jenis Laporan");
        jPanel2.add(jLabel1);

        jenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Rekap Presensi per Unit", "Rekap Presensi Pejabat Struktural", "Perhitungan Uang Makan", "Rekap Potongan Tunjangan", "Detail Presensi", "Kuitansi Perhitungan Uang Makan", "Rekap Presensi Semua Unit", "Detail Presensi Skip WeekEnd" }));
        jenis.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jenisItemStateChanged(evt);
            }
        });
        jenis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jenisActionPerformed(evt);
            }
        });
        jPanel2.add(jenis);

        tipe.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PNS", "BUKAN PNS" }));
        jPanel2.add(tipe);

        labelUnit.setText("Unit Kerja");
        jPanel2.add(labelUnit);

        unit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Rekap Presensi", "Detail Presensi" }));
        jPanel2.add(unit);

        jLabel2.setText("Periode");
        jPanel2.add(jLabel2);

        periode_bulan.setYearChooser(periode_tahun);
        jPanel2.add(periode_bulan);
        jPanel2.add(periode_tahun);

        jLabel3.setText("Tanggal Presensi");
        jPanel2.add(jLabel3);

        tanggal_awal.setDate(new java.util.Date());
        tanggal_awal.setDateFormatString("dd/MM/yyyy");
        jPanel2.add(tanggal_awal);

        jLabel4.setText("s/d");
        jPanel2.add(jLabel4);

        tanggal_akhir.setDate(new java.util.Date());
        tanggal_akhir.setDateFormatString("dd/MM/yyyy");
        jPanel2.add(tanggal_akhir);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/report.png"))); // NOI18N
        jButton1.setText("Tampil Laporan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);

        jPanel1.add(jPanel2, java.awt.BorderLayout.CENTER);

        xxxyy.add(jPanel1, java.awt.BorderLayout.NORTH);

        getContentPane().add(xxxyy);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        showReport();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jenisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jenisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jenisActionPerformed

    private void jenisItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jenisItemStateChanged
        int p = jenis.getSelectedIndex();
        if (p == 2 || p == 5) {
            tipe.setVisible(true);
            labelUnit.setVisible(false);
            unit.setVisible(false);
        } else if (p == 1 || p == 6) {
            tipe.setVisible(false);
            labelUnit.setVisible(false);
            unit.setVisible(false);
        } else {
            tipe.setVisible(false);
            labelUnit.setVisible(true);
            unit.setVisible(true);
        }
    }//GEN-LAST:event_jenisItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel data;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JComboBox<String> jenis;
    private javax.swing.JLabel labelUnit;
    private com.toedter.calendar.JMonthChooser periode_bulan;
    private com.toedter.calendar.JYearChooser periode_tahun;
    private com.toedter.calendar.JDateChooser tanggal_akhir;
    private com.toedter.calendar.JDateChooser tanggal_awal;
    private javax.swing.JComboBox<String> tipe;
    private javax.swing.JComboBox<String> unit;
    private javax.swing.JPanel xxxyy;
    // End of variables declaration//GEN-END:variables
}
