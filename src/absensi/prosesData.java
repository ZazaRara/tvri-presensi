/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absensi;

import java.sql.Connection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class prosesData extends javax.swing.JInternalFrame {

    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    String user = "";
    Connection kon;
    TaskWorker task;

    /**
     * Creates new form prosesData
     */
    public prosesData(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        //     atas.removeAll();
        //     atas.add(panelFilter);
        tglTarik();
        setJamAbsensi();
    }

    private void tglTarik() {
        String sql = "SELECT MAX(tanggal) FROM tvri_pegawai_data_absensi WHERE asn IS NOT NULL";
        String tgl = komp.getStringSQL(kon, sql);
        tgl_last.setText(tgl);
        
    }

    private synchronized void hapusDataAbsensi(String awal, String akhir) {
        String sql = "DELETE FROM tvri_pegawai_data_absensi WHERE tanggal BETWEEN '" + awal + "' AND '" + akhir + "' AND asn IS NOT NULL";
        komp.setSQL(kon, sql);
    }

    private void setJamAbsensi() {
        String sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'PAGI-IN'";
        Object[] data = komp.setDataEdit(kon, sql);
        j_pagi_in_awal.setText(data[0].toString());
        j_pagi_in_akhir.setText(data[1].toString());

        sql = "SELECT jam_min FROM tvri_master_jam WHERE id = 'PAGI-OUT'";
        data = komp.setDataEdit(kon, sql);
        j_batas_masuk.setText(data[0].toString());
        //   j_pagi_out_akhir.setText(data[1].toString());

        sql = "SELECT jam_min, jam_max FROM tvri_master_jam WHERE id = 'SIANG-OUT'";
        data = komp.setDataEdit(kon, sql);
        j_siang_in_awal.setText(data[0].toString());
        j_siang_in_akhir.setText(data[1].toString());

        sql = "SELECT jam_min FROM tvri_master_jam WHERE id = 'SIANG-IN'";
        data = komp.setDataEdit(kon, sql);
        j_batas_pulang.setText(data[0].toString());
        //    j_siang_out_akhir.setText(data[1].toString());

        sql = "SELECT jam_min FROM tvri_master_jam WHERE id = 'MALAM-IN'";
        data = komp.setDataEdit(kon, sql);
        j_batas_operasional.setText(data[0].toString());
        //   j_malam_in_akhir.setText(data[1].toString());

    }

    private synchronized void simpanAbsensi() {
        int yy = tabelData.getRowCount();
        //    System.out.println(yy);
        int gagal = 0;
        int sukses = 0;
        int skip = 0;
        for (int x = 0; x < yy; x++) {
            String a = tabelData.getValueAt(x, 1).toString();
            String b = tabelData.getValueAt(x, 2).toString();
            //    String c = tabelData.getValueAt(x, 3).toString();
            String tanggal = tabelData.getValueAt(x, 4).toString();
            String masuk = tabelData.getValueAt(x, 5).toString();
            String pulang = tabelData.getValueAt(x, 6).toString();
            String durasi = tabelData.getValueAt(x, 7).toString();
            String status = tabelData.getValueAt(x, 8).toString();
            String keterangan = tabelData.getValueAt(x, 9).toString();
            String gasik = tabelData.getValueAt(x, 10).toString();
            int ada = komp.getIntSQL(kon, "SELECT count(1) FROM tvri_pegawai_data_absensi WHERE id_pegawai = '" + a + "' AND tanggal = '" + tanggal + "'");
            if (ada == 0) {
                String save = "INSERT INTO tvri_pegawai_data_absensi"
                        + "(id_pegawai, no_absen, tanggal, jam_pagi, "
                        + "jam_sore, asn, status_absensi, keterangan, sort_tanggal, from_date, jam_siang) "
                        + "VALUES('" + a + "','" + b + "','" + tanggal + "',"
                        + "'" + masuk + "','" + pulang + "','" + durasi + "','" + status + "','" + keterangan + "','" + tanggal.replaceAll("-", "") + "',now(),'" + gasik + "')";
                //    System.out.println(save);
                boolean ok = komp.setSQL(kon, save);
                if (ok) {
                    sukses++;
                } else {
                    gagal++;
                }
            } else {
                skip++;
            }
        }
        pesan.pesanSukses("Sukses Simpan Data Presensi", "<br />Total Data :" + yy + "<br />Sukses : " + sukses + "<br />Gagal : " + gagal+ "<br />Skip : " + skip);
        komp.hapusTabel(tabelData);
        tglTarik();
    }

    private void updateJamAbsensi() {

        String a = j_pagi_in_awal.getText();
        String b = j_pagi_in_akhir.getText();
        String c = "PAGI-IN";
        String sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        //BATAS MASUK
        a = j_batas_masuk.getText();
        //     b = j_pagi_out_akhir.getText();
        c = "PAGI-OUT";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        // PULANG
        a = j_siang_in_awal.getText();
        b = j_siang_in_akhir.getText();
        c = "SIANG-OUT";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        // MULAI PULANG
        a = j_batas_pulang.getText();
        //    b = j_siang_out_akhir.getText();
        c = "SIANG-IN";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

        // MULAI OPERASIONAL
        a = j_batas_operasional.getText();
        //     b = j_malam_in_akhir.getText();
        c = "MALAM-IN";
        sql = "UPDATE tvri_master_jam SET jam_min = '" + a + "', jam_max = '" + b + "' WHERE id = '" + c + "'";
        komp.setSQL(kon, sql);

    }

    private void prosesDataAbsensi() {
        String awal = new java.sql.Date(tglAwal.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tglAkhir.getDate().getTime()).toString();
        String masuk = j_batas_masuk.getText();
        String pulang = j_batas_pulang.getText();
        String opr = j_batas_operasional.getText();
        String awal_masuk = j_pagi_in_awal.getText();
        String akhir_masuk = j_pagi_in_akhir.getText();
        String awal_pulang = j_siang_in_awal.getText();
        String akhir_pulang = j_siang_in_akhir.getText();
        updateJamAbsensi();
        if (cek.isSelected()) {
            hapusDataAbsensi(awal, akhir);
        }
        String sql = "SELECT peg.emp_id_auto as id_pegawai, a.pin,CONCAT(peg.first_name,' ', peg.last_name) as nama, a.tgl, IFNULL(awal.jam,'-')  as jam_masuk, IFNULL(jeda.jam, '-') as jam_pulang, \n"
                + "IFNULL((jeda.jam  - awal.jam) / 10000, 0) as durasi, \n"
                + "CASE WHEN awal.jam IS NULL AND jeda.jam IS NULL THEN 'Tidak Masuk'\n"
                + "WHEN awal.jam IS NULL OR jeda.jam IS NULL THEN 'Absen 1 Kali'\n"
                + "ELSE 'Masuk' END\n"
                + " as status_absensi,\n"
                + "CASE WHEN awal.jam IS NULL THEN 'Tidak Absen Masuk'\n"
                + "WHEN jeda.jam IS NULL THEN 'Tidak Absen Pulang'\n"
                + "WHEN awal.jam > '" + masuk + "' AND jeda.jam <'" + pulang + "' THEN 'Masuk Terlambat & Pulang Cepat'\n"
                + "WHEN awal.jam > '" + masuk + "' THEN 'Masuk Terlambat'\n"
                + "WHEN jeda.jam < '" + pulang + "' THEN 'Pulang Cepat'\n"
                + "WHEN jeda.jam > '" + opr + "' THEN 'Operasional'\n"
                + "ELSE ''\n"
                + "END as alasan, gasik.jam as gasik\n"
                + "FROM (\n"
                + "    SELECT DISTINCT pin, cast(scan_date as date) as tgl, \n"
                + "      CONCAT(cast(scan_date as date), pin) as kodetgl \n"
                + "       FROM att_log \n"
                + "       WHERE cast(scan_date as date) BETWEEN '" + awal + "' AND '" + akhir + "' \n"
                + "       ORDER BY pin, scan_date\n"
                + "    ) a\n"
                + "LEFT JOIN (\n"
                + "-- MASUK\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "-- pin = 2 AND \n"
                + "cast(scan_date as date) BETWEEN '" + awal + "' AND '" + akhir + "' AND cast(scan_date as time) BETWEEN '" + awal_masuk + "' AND '" + akhir_masuk + "' ORDER BY pin, scan_date\n"
                + ") as awal ON awal.pin = a.pin AND awal.tgl = a.kodetgl AND awal.RowNumber = 1\n"
                + "-- PULANG\n"
                + "LEFT JOIN (\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "-- pin = 2 AND \n"
                + "cast(scan_date as date) BETWEEN '" + awal + "' AND '" + akhir + "' AND cast(scan_date as time) BETWEEN '" + awal_pulang + "' AND '" + akhir_pulang + "' ORDER BY pin, scan_date desc\n"
                + ") as jeda ON jeda.pin = a.pin AND jeda.tgl = a.kodetgl AND jeda.RowNumber = 1\n"
                + "LEFT JOIN (\n"
                + "-- GASIK\n"
                + "SELECT pin, cast(scan_date as time) as jam,\n"
                + "@cur:= IF(CONCAT(cast(scan_date as date),pin)=@id, @cur+1, 1) AS RowNumber, \n"
                + "  @id := CONCAT(cast(scan_date as date),pin) as tgl\n"
                + "FROM att_log \n"
                + "CROSS JOIN \n"
                + "    (SELECT @id:=(SELECT MIN(CONCAT(cast(scan_date as date), pin)) FROM att_log), @cur:=0) AS init\n"
                + "WHERE \n"
                + "-- pin = 2 AND \n"
                + "cast(scan_date as date) BETWEEN '" + awal + "' AND '" + akhir + "' AND cast(scan_date as time) BETWEEN '05:00' AND '" + akhir_masuk + "' ORDER BY pin, scan_date\n"
                + ") as gasik ON gasik.pin = a.pin AND gasik.tgl = a.kodetgl AND gasik.RowNumber = 1\n"
                + "LEFT JOIN emp peg ON peg.pin = a.pin\n"
                + "WHERE awal.jam IS NOT NULL OR jeda.jam IS NOT NULL";
        //     String save = "INSERT INTO tvri_pegawai_data_absensi(id_pegawai, keterangan, no_absen, tanggal, jam_pagi, jam_siang, jam_sore, jam_malam, asn, non_asn) " + sql;
        //     System.out.println(save);
        //     komp.setSQL(kon, save);

        komp.setDataTabel(kon, tabelData, sql, 1);
        //   simpanAbsensi();
        int yy = tabelData.getRowCount();
        if (yy == 0) {
            pesan.pesanError("Tidak Ada data Presensi", "Tanggal " + awal + " s/d " + akhir + "", "Silahkan pilih tanggal lainnya");
        } else {
            pesan.pesanSukses("Sukses Proses Data Presensi", "Tanggal " + awal + " s/d " + akhir + "<br />Total Data :" + yy);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        xxxyy = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        atas = new javax.swing.JPanel();
        panelFilter = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tglAkhir = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        j_pagi_in_akhir = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        j_pagi_in_awal = new javax.swing.JTextField();
        tglAwal = new com.toedter.calendar.JDateChooser();
        j_siang_in_awal = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        j_siang_in_akhir = new javax.swing.JTextField();
        j_batas_masuk = new javax.swing.JTextField();
        j_batas_pulang = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        j_batas_operasional = new javax.swing.JTextField();
        cek = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        tgl_last = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();

        setTitle("Proses Data");
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        xxxyy.setBackground(new java.awt.Color(255, 255, 255));
        xxxyy.setLayout(new java.awt.BorderLayout());

        data.setOpaque(false);
        data.setLayout(new java.awt.GridLayout(1, 0));

        jScrollPane1.setOpaque(false);

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "PIN", "Nama Pegawai", "Tanggal", "Scan Masuk", "Scan Pulang", "Durasi", "Status", "Keterangan", "data"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelData.setOpaque(false);
        jScrollPane1.setViewportView(tabelData);

        data.add(jScrollPane1);

        xxxyy.add(data, java.awt.BorderLayout.CENTER);

        atas.setOpaque(false);
        atas.setLayout(new java.awt.GridLayout(1, 0));

        panelFilter.setOpaque(false);
        panelFilter.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        panelFilter.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 0, 70, 150));

        tglAkhir.setDate(new Date());
        tglAkhir.setDateFormatString("dd/MM/yyy");
        panelFilter.add(tglAkhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, 90, -1));

        jLabel2.setText("s/d");
        panelFilter.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 40, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Mulai Pulang");
        panelFilter.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 70, 90, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText(":");
        panelFilter.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 70, 20, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Scan Masuk");
        panelFilter.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 90, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Scan Pulang");
        panelFilter.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 90, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Batas Masuk");
        panelFilter.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 40, 100, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Tanggal");
        panelFilter.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 90, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText(":");
        panelFilter.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 20, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText(":");
        panelFilter.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 20, 20));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText(":");
        panelFilter.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 20, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText(":");
        panelFilter.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 40, 20, 20));

        j_pagi_in_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_in_akhir.setText("12:00");
        panelFilter.add(j_pagi_in_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 90, -1));

        jLabel13.setText("s/d");
        panelFilter.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, -1, 20));

        j_pagi_in_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_pagi_in_awal.setText("07:30");
        panelFilter.add(j_pagi_in_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 90, -1));

        tglAwal.setDate(new Date());
        tglAwal.setDateFormatString("dd/MM/yyy");
        panelFilter.add(tglAwal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 90, -1));

        j_siang_in_awal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_in_awal.setText("12:01");
        j_siang_in_awal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j_siang_in_awalActionPerformed(evt);
            }
        });
        panelFilter.add(j_siang_in_awal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 90, -1));

        jLabel14.setText("s/d");
        panelFilter.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 70, -1, 20));

        j_siang_in_akhir.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_siang_in_akhir.setText("23:59");
        panelFilter.add(j_siang_in_akhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, 90, -1));

        j_batas_masuk.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_batas_masuk.setText("09:00");
        panelFilter.add(j_batas_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 40, 90, -1));

        j_batas_pulang.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_batas_pulang.setText("16:00");
        panelFilter.add(j_batas_pulang, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 70, 90, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/order.png"))); // NOI18N
        jButton1.setText("Proses Data Presensi");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panelFilter.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 110, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Mulai Operasional");
        panelFilter.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 70, 130, 20));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText(":");
        panelFilter.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 70, 20, 20));

        j_batas_operasional.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        j_batas_operasional.setText("17:00");
        panelFilter.add(j_batas_operasional, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 70, 90, -1));

        cek.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cek.setText("Hapus data Lama");
        cek.setOpaque(false);
        panelFilter.add(cek, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, -1, -1));

        jLabel17.setText("Terakhir Proses Pada Tanggal :");
        panelFilter.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 160, -1));

        tgl_last.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tgl_last.setText("jLabel18");
        panelFilter.add(tgl_last, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, 150, -1));

        atas.add(panelFilter);

        xxxyy.add(atas, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 5));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton2.setText("SIMPAN HASIL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton2);

        xxxyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setOpaque(false);
        xxxyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        xxxyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        getContentPane().add(xxxyy);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //   atas.removeAll();
        //   atas.add(panelLoading);
        task = new TaskWorker();
        task.execute();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        simpanAbsensi();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void j_siang_in_awalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j_siang_in_awalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j_siang_in_awalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel atas;
    private javax.swing.JCheckBox cek;
    private javax.swing.JPanel data;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField j_batas_masuk;
    private javax.swing.JTextField j_batas_operasional;
    private javax.swing.JTextField j_batas_pulang;
    private javax.swing.JTextField j_pagi_in_akhir;
    private javax.swing.JTextField j_pagi_in_awal;
    private javax.swing.JTextField j_siang_in_akhir;
    private javax.swing.JTextField j_siang_in_awal;
    private javax.swing.JPanel panelFilter;
    private javax.swing.JTable tabelData;
    private com.toedter.calendar.JDateChooser tglAkhir;
    private com.toedter.calendar.JDateChooser tglAwal;
    private javax.swing.JLabel tgl_last;
    private javax.swing.JPanel xxxyy;
    // End of variables declaration//GEN-END:variables

    class TaskWorker extends SwingWorker<Void, Void> {

        /*
     * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {

            prosesDataAbsensi();
            return null;
        }

        /*
     * Executed in event dispatching thread
         */
        @Override
        public void done() {
            //    atas.removeAll();
            //    atas.add(panelFilter);

        }
    }
}
